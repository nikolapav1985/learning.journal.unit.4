Learning journal unit 4
-----------------------

- hyp.py (compute hypotenuse of right angled triangle, check comments for details)
- components.py (compute horizontal and vertical components of a force, check comments for details)

Run examples
-------------

- ./hyp.py 3 4 (or some other values)
- ./components 50 0.79 (or some other values)

Test environment
----------------

OS lubuntu 16.04 64bit lts kernel version 4.13.0 python version 2.7.12
