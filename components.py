#!/usr/bin/python

import math
import sys

"""
FILE components.py
"""

def horizontal(f,theta):
    """
        procedure horizontal

        compute horizontal component of a force

        parameters

        f(float) resulting force
        theta(float) angle in radians from horizontal line

        STAGE A

            Compute cosine of an angle.

            TESTS

            INPUT 50 0.79
            OUTPUT 0.70
            
            COMMENTS

                Compute cosine of an angle (45 degrees, 0.79 radians). Output is correct.

            CODE
                cs=math.cos(theta)

        STAGE B

            Compute horizontal component of a force.

            TESTS

            INPUT 50 0.79
            OUTPUT 35.00
            
            COMMENTS

                There is correct output for given arguments.

            CODE

                hr=f*cs (force time cosine of an angle)
        STAGE C

            Return result of computation.

            TESTS

            INPUT 50 0.79
            OUTPUT 35.00
            
            COMMENTS

                Correct result is returned from this function. This result can be further utilized.

            CODE

                return hr

        TESTS

        INPUT 20 0.52
        OUTPUT 17.35 9.94
        COMMENTS

            As an input there is resulting force of 20 N acting under angle of 30 degrees (0.52 radians, in terms of horizontal line). Output for horizontal component of 17.35 N is correct.

        INPUT 50 0.44
        OUTPUT 45.23 21.29
        COMMENTS

            As an input there is resulting force of 50 N acting under angle of 20 degrees (0.44 radians, in terms of horizontal line). Output for horizontal component of 45.23 N is correct.

        INPUT 50 0.79
        OUTPUT 35.19 35.52
        COMMENTS

            As an input there is resulting force of 50 N acting under angle of 45 degrees (0.79 radians, in terms of horizontal line). Output for horizontal component of 35.19 N is correct. Horizontal and vertical components are almost same.

        PEER ASSESSMENT

            In terms of received peer assessment, it is pretty BAD so far. Comments are often vague, missing, false or not related to actual content. Grades too. Discussion forum content varies greatly. There are some good responses and some  pretty far from topic being discussed. It is same for PROGRAMMING ASSIGNMENTS. Feedback is INCOMPLETE, given by zero, one or couple of peers. Never given by three peers (as it should be). Also feedback and grades are far from GRADING GUIDE. At this moment I could use COURSE INSTRUCTOR to go over content, check again and re-grade. I may possibly need to appeal grades too having backed up all content so far. In terms of given peer assessment, I feel pretty good, because I stick to grading guides.
    """
    cs=math.cos(theta)
    hr=f*cs
    return hr

def vertical(f,theta):
    """
        procedure vertical

        compute vertical component of a force

        parameters

        f(float) resulting force
        theta(float) angle in radians from horizontal line (vertical line is opposite side of an angle)
    """
    s=math.sin(theta)
    vr=f*s
    return vr

if __name__=="__main__":
    if len(sys.argv) <> 3:
        sys.exit(1)
    f=float(sys.argv[1])
    theta=float(sys.argv[2])
    print horizontal(f,theta)
    print vertical(f,theta)
