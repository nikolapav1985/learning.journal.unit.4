#!/usr/bin/python

import math
import sys

"""
FILE hyp.py
"""

def hypotenuse(a,b):
    """
        procedure hypotenuse

        find hypotenuse of right angled triangle

        parameters

        a(float) one side of a triangle
        b(float) another side of a triangle

        STAGE A find a^2 (a squared) and b^2 (b squared), can be done by using simple multiplication

            TESTS

            INPUT 3 4
            OUTPUT 9 16
            
            COMMENTS
            
                Compute 3 squared (9) and 4 squared (16). This is done correctly.    

            CODE
                asq=a*a
                bsq=b*b

        STAGE B import appropriate package for square root function and find sqare root (math package)

            TESTS

            INPUT 3 4
            OUTPUT 5
            
            COMMENTS
            
                Compute square root of sum of 3 squared (9) and 4 squared (16). This is done correctly. (3,4,5) is one of Pythagorean triples (all sides of a right angle triangle are integers).   

            CODE
                rt=math.sqrt(asq+bsq)

        STAGE C return correct result (and print result)

            TESTS

            INPUT 3 4
            OUTPUT 5
            
            COMMENTS

                Return result of computation, 5 in this case. Once a value is returned, print this to standard output.

            CODE
                return rt
        
        TESTS

        INPUT 3 4
        OUTPUT 5

        INPUT 9 40
        OUTPUT 41

        INPUT 20 41
        OUTPUT 29
    """
    asq=a*a
    """
    print(asq)
    """
    bsq=b*b
    """
    print(bsq)
    """
    rt=math.sqrt(asq+bsq)
    """
    print(rt)
    """
    return rt

if __name__=="__main__":
    if len(sys.argv) <> 3:
        sys.exit(1)
    a=int(sys.argv[1])
    b=int(sys.argv[2])
    print hypotenuse(a,b)
